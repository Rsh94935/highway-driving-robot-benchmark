from vehicle import Driver

#Developed by rh18abr of Hertfordshire University
#Python script to run the Highway Driving Robot Benchmark project

sensorsNames = [
    "front",
    "front right 0",
    "front right 1",
    "front right 2",
    "front left 0",
    "front left 1",
    "front left 2",
    "rear",
    "rear left",
    "rear right",
    "right",
    "left"]
sensors = {}
#Multi-dimensional array, top level is each lane, from left to right, second level is the distances from the barriers (Left, Right)
laneDistances = [[2.0, 10.0], [6.0, 9.75], [10.0, 5.75]]
#Min distance allowed between car's in lanes
vehicleDistane = 1.75
currentLane = 2
movingFrom = None
maxSpeed = 75
safeOvertake = False
balancer = 0
priorDiff = None

def overtake(lane):
    global priorDiff
    #Whilst this variable is only applied in this function, want it as global to ensure that the value is remembered
    global balancer
    global vehicleDistance
    targetDistance = 0
    currentDistance = 0
    right = sensors['right'].getValue()
    left = sensors['left'].getValue()
    #Ensure that the vehicle is not too close to the side lane's vehicle
    if lane[0] < vehicleDistance:
        return (left - vehicleDistance) * 0.005
    elif lane[1] < vehicleDistance:
        return (right - vehicleDistance) * 0.005
    leftDistanceDiff = (lane[0] - left)
    rightDistanceDiff = (right - lane[1])
    
    diff = leftDistanceDiff + rightDistanceDiff
    # balancing mechanism
    if diff > 0 and priorDiff < 0:
        balancer = 0
    if diff < 0 and priorDiff > 0:
        balancer = 0
    balancer += diff
    if priorDiff is None:
        priorDiff = diff
        
    #Calculate angle to be returned
    angle = 0.05 * diff + balancer * 0.00001 + 20 * (diff - priorDiff)
    priorDiff = diff
    
    return angle

#Check whether right lane is safe to move into
def checkRight():
    global currentLane
    frontRight = sensors['front right 1'].getValue()
    right = sensors['right'].getValue()
    rearRight = sensors['rear right'].getValue()
    
    if frontRight >= 10.0 and right > 7.5 and rearRight > 1 and currentLane != 2:
        return True
    else:
        return False

#Check whether left lane is safe to move into
def checkLeft():
    global currentLane
    frontLeft0 = sensors['front left 0'].getValue()
    frontLeft1 = sensors['front left 1'].getValue()
    frontLeft2 = sensors['front left 2'].getValue()
    left = sensors['left'].getValue()
    rearLeft = sensors['rear left'].getValue()
    
    if frontLeft0 >= 10.0 and frontLeft1 >= 10.0 and frontLeft2 >= 10.0 and left > 7.5 and rearLeft > 1 and currentLane != 0:
        return True
    else:
        return False
    
driver = Driver()
driver.setSteeringAngle(0.0)  # go straight

for name in sensorsNames:
    sensors[name] = driver.getDistanceSensor("distance sensor " + name)
    sensors[name].enable(10)


while driver.step() != -1:
    # adjust speed according to front vehicle
    frontDistance = sensors["front"].getValue()
    frontRange = sensors["front"].getMaxValue()
    speed = maxSpeed * frontDistance / frontRange
    #Need to check for vehicles moving into my lane
    if sensors["front right 0"].getValue() < 5.0 or sensors["front left 0"].getValue() < 5.0:
        speed = min(0.5 * maxSpeed, speed)
    driver.setCruisingSpeed(speed)
    #If car is infront, slow down based on proximity
    speedDiff = driver.getCurrentSpeed() - speed
    if speedDiff > 0:
        driver.setBrakeIntensity(min(speedDiff / speed, 1))
    else:
        driver.setBrakeIntensity(0)
    #Car in front, try to overtake, set lane params
    #Always try to be in furthest right lane, otherwise check whether there is a vehicle infront and then undertake
    if (checkRight() and movingFrom is None):
        currentLane += 1
        movingFrom = 'left'
    elif frontDistance < frontRange and movingFrom is None:
        if (checkLeft()):
            currentLane -= 1
            print(currentLane)
            movingFrom = 'right'
    #Steer into middle of lane
    angle = max(min(overtake(laneDistances[currentLane]), 0.05), -0.05)
    driver.setSteeringAngle(angle)
    
    #If overtaking is finished, set to no longer overtaking
    if round(laneDistances[currentLane][0]) == round(sensors['left'].getValue()) and round(laneDistances[currentLane][1]) == round(sensors['right'].getValue()) and movingFrom != None:
        movingFrom = None